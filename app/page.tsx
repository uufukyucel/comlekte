import Products from "@/components/Products";
import api from "@/api/menu.json";
import { Product } from "@/types/Product";

export default function Home() {
    return (
        <main>
            <Products products={api.products as Product[]} />
        </main>
    );
}

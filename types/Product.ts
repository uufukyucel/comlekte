export interface ProductItem {
    name: string;
    price: number;
    image: string;
}

export interface Product {
    title: string;
    items: ProductItem[];
}

export interface ProductsProps {
    products: Product[];
}
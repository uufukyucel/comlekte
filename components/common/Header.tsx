function Header(){
    return (
        <header>
            <h2 className="text-[#686753] text-3xl">Hoşgeldiniz</h2>
            <span>Ne sipariş etmek istersiniz?</span>
        </header>
    )
}

export default Header;
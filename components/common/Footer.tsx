function Footer(){
    return (
        <footer className="flex items-center justify-center h-16 flex-col mt-6">
            Çömlek'te &copy; {new Date().getFullYear()}
            <span className="text-xs">Designed by Yuceloper</span>
        </footer>
    )
}
export default Footer;
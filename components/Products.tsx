import React from "react";
import Image from 'next/image';
import { ProductsProps } from "@/types/Product";

const Products: React.FC<ProductsProps> = ({ products }) => {
    return (
        <div>
            {products && products.map((pitem, pindex) => (
                <div key={pindex} className="mt-4">
                    <h4 className="text-[#686753] text-lg">{pitem.title}</h4>
                    <ul className="flex mt-2.5 gap-4">
                        <div className="overflow-auto flex">
                            {pitem.items && pitem.items.map((item, index) => (
                                <li key={index} className="flex items-center justify-between flex-col bg-[#f0f1f3] rounded-xl p-2 min-w-[140px] max-w-[140px] min-h-[160px] max-h-[160px] mr-2.5">
                                    <div className="w-[72px] h-[90px] flex items-center justify-center rounded-full overflow-hidden">
                                        <Image src={item.image} alt={item.name} width={70} height={70} className="rounded-full" />
                                    </div>
                                    <span className="text-xs text-center font-semibold">{item.name}</span>
                                    <span className="text-[#686753]">{item.price ?? '-'} ₺</span>
                                </li>
                            ))}
                        </div>
                    </ul>
                </div>
            ))}
        </div>
    );
}

export default Products;
